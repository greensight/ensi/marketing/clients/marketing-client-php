<?php
/**
 * DiscountCreateIncludesProductTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\MarketingClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. Marketing.
 *
 * Маркетинг
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\MarketingClient;

use PHPUnit\Framework\TestCase;

/**
 * DiscountCreateIncludesProductTest Class Doc Comment
 *
 * @category    Class
 * @description DiscountCreateIncludesProduct
 * @package     Ensi\MarketingClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class DiscountCreateIncludesProductTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "DiscountCreateIncludesProduct"
     */
    public function testDiscountCreateIncludesProduct()
    {
    }

    /**
     * Test attribute "product_id"
     */
    public function testPropertyProductId()
    {
    }
}

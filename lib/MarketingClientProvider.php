<?php

namespace Ensi\MarketingClient;

class MarketingClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\MarketingClient\Api\CalculatorsApi',
        '\Ensi\MarketingClient\Api\DiscountProductsApi',
        '\Ensi\MarketingClient\Api\DiscountRelationsApi',
        '\Ensi\MarketingClient\Api\DiscountsApi',
        '\Ensi\MarketingClient\Api\PromoCodesApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\MarketingClient\Dto\CalculateCatalogOffer',
        '\Ensi\MarketingClient\Dto\CalculateCatalogRequest',
        '\Ensi\MarketingClient\Dto\CalculateCatalogResponse',
        '\Ensi\MarketingClient\Dto\CalculateCatalogResponseData',
        '\Ensi\MarketingClient\Dto\CalculateCheckoutOffer',
        '\Ensi\MarketingClient\Dto\CalculateCheckoutRequest',
        '\Ensi\MarketingClient\Dto\CalculateCheckoutResponse',
        '\Ensi\MarketingClient\Dto\CalculateCheckoutResponseData',
        '\Ensi\MarketingClient\Dto\CalculatedDiscount',
        '\Ensi\MarketingClient\Dto\CalculatedOffer',
        '\Ensi\MarketingClient\Dto\CheckPromoCodeRequest',
        '\Ensi\MarketingClient\Dto\CheckPromoCodeResponse',
        '\Ensi\MarketingClient\Dto\CheckPromoCodeResponseData',
        '\Ensi\MarketingClient\Dto\CreateDiscountRequest',
        '\Ensi\MarketingClient\Dto\CreatePromoCodeRequest',
        '\Ensi\MarketingClient\Dto\DeleteDiscountProductsRequest',
        '\Ensi\MarketingClient\Dto\Discount',
        '\Ensi\MarketingClient\Dto\DiscountCreateIncludesProduct',
        '\Ensi\MarketingClient\Dto\DiscountCreateIncludesProductsProperties',
        '\Ensi\MarketingClient\Dto\DiscountCreateProperties',
        '\Ensi\MarketingClient\Dto\DiscountFillableProperties',
        '\Ensi\MarketingClient\Dto\DiscountIncludes',
        '\Ensi\MarketingClient\Dto\DiscountProduct',
        '\Ensi\MarketingClient\Dto\DiscountProductFillableLikeIncludeProperties',
        '\Ensi\MarketingClient\Dto\DiscountProductFillableProperties',
        '\Ensi\MarketingClient\Dto\DiscountProductReadonlyProperties',
        '\Ensi\MarketingClient\Dto\DiscountProductResponse',
        '\Ensi\MarketingClient\Dto\DiscountReadonlyProperties',
        '\Ensi\MarketingClient\Dto\DiscountResponse',
        '\Ensi\MarketingClient\Dto\DiscountStatusEnum',
        '\Ensi\MarketingClient\Dto\DiscountTypeEnum',
        '\Ensi\MarketingClient\Dto\DiscountValueTypeEnum',
        '\Ensi\MarketingClient\Dto\EditDiscountProduct',
        '\Ensi\MarketingClient\Dto\EmptyDataResponse',
        '\Ensi\MarketingClient\Dto\Error',
        '\Ensi\MarketingClient\Dto\ErrorResponse',
        '\Ensi\MarketingClient\Dto\ErrorResponse2',
        '\Ensi\MarketingClient\Dto\IdsObject',
        '\Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest',
        '\Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequestAllOf',
        '\Ensi\MarketingClient\Dto\ModelInterface',
        '\Ensi\MarketingClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\MarketingClient\Dto\PaginationTypeEnum',
        '\Ensi\MarketingClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\MarketingClient\Dto\PatchDiscountProductsRequest',
        '\Ensi\MarketingClient\Dto\PatchDiscountRequest',
        '\Ensi\MarketingClient\Dto\PatchPromoCodeRequest',
        '\Ensi\MarketingClient\Dto\PromoCode',
        '\Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum',
        '\Ensi\MarketingClient\Dto\PromoCodeFillableProperties',
        '\Ensi\MarketingClient\Dto\PromoCodeIncludes',
        '\Ensi\MarketingClient\Dto\PromoCodeReadonlyProperties',
        '\Ensi\MarketingClient\Dto\PromoCodeResponse',
        '\Ensi\MarketingClient\Dto\PromoCodeStatusEnum',
        '\Ensi\MarketingClient\Dto\RequestBodyCursorPagination',
        '\Ensi\MarketingClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\MarketingClient\Dto\RequestBodyPagination',
        '\Ensi\MarketingClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\MarketingClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\MarketingClient\Dto\ResponseBodyPagination',
        '\Ensi\MarketingClient\Dto\SearchDiscountProductsRequest',
        '\Ensi\MarketingClient\Dto\SearchDiscountProductsResponse',
        '\Ensi\MarketingClient\Dto\SearchDiscountsRequest',
        '\Ensi\MarketingClient\Dto\SearchDiscountsResponse',
        '\Ensi\MarketingClient\Dto\SearchDiscountsResponseMeta',
        '\Ensi\MarketingClient\Dto\SearchPromoCodesRequest',
        '\Ensi\MarketingClient\Dto\SearchPromoCodesResponse',
    ];

    /** @var string */
    public static $configuration = '\Ensi\MarketingClient\Configuration';
}

# # CalculateCheckoutResponseData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offers** | [**\Ensi\MarketingClient\Dto\CalculatedOffer[]**](CalculatedOffer.md) |  | 
**promo_code** | **string** | Использованный промокод | 
**promo_code_apply_status** | **string** | Статус применения промокода | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # CalculateCheckoutRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offers** | [**\Ensi\MarketingClient\Dto\CalculateCheckoutOffer[]**](CalculateCheckoutOffer.md) |  | 
**promo_code** | **string** | Введенный промокод | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # DiscountReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания скидки | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления скидки | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



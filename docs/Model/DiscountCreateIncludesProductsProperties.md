# # DiscountCreateIncludesProductsProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\Ensi\MarketingClient\Dto\DiscountCreateIncludesProduct[]**](DiscountCreateIncludesProduct.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



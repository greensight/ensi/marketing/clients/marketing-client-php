# # PromoCodeIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount** | [**\Ensi\MarketingClient\Dto\Discount**](Discount.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



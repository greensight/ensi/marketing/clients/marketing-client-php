# # PromoCodeFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** | статус промокода из PromoCodeStatusEnum | [optional] 
**name** | **string** | Название промокода | [optional] 
**code** | **string** | Код | [optional] 
**counter** | **int** | Ограничение на количество использований промокода | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | Дата начала действия промокода | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | Дата окончания действия промокода | [optional] 
**discount_id** | **int** | ID скидки | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



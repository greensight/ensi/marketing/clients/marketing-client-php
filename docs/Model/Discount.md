# # Discount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания скидки | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления скидки | 
**type** | **int** | Тип скидки из DiscountTypeEnum | 
**name** | **string** | Наименование скидки | [optional] 
**value_type** | **int** | Тип значения скидки из DiscountValueTypeEnum | [optional] 
**value** | **int** | Размер скидки | [optional] 
**status** | **int** | Статус скидки из DiscountStatusEnum | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | Дата начала действия скидки | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | Дата окончания действия скидки | [optional] 
**promo_code_only** | **bool** | Скидка действительна только по промокоду | [optional] 
**products** | [**\Ensi\MarketingClient\Dto\DiscountProduct[]**](DiscountProduct.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # CalculateCheckoutOffer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_id** | **int** | ID оффера | 
**product_id** | **int** | ID товара | 
**cost** | **int** | Цена без скидки (коп.) | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



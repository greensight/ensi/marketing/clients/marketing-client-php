# # PromoCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**current_counter** | **int** | Количество использований промокода | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**status** | **int** | статус промокода из PromoCodeStatusEnum | [optional] 
**name** | **string** | Название промокода | [optional] 
**code** | **string** | Код | [optional] 
**counter** | **int** | Ограничение на количество использований промокода | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | Дата начала действия промокода | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | Дата окончания действия промокода | [optional] 
**discount_id** | **int** | ID скидки | [optional] 
**discount** | [**\Ensi\MarketingClient\Dto\Discount**](Discount.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



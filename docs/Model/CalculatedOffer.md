# # CalculatedOffer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_id** | **int** | ID оффера | 
**cost** | **int** | Цена без скидки (коп.) | 
**price** | **int** | Цена с учетом скидки (коп.) | 
**discounts** | [**\Ensi\MarketingClient\Dto\CalculatedDiscount[]**](CalculatedDiscount.md) | доступные скидки по офферу | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# Ensi\MarketingClient\DiscountProductsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDiscountProduct**](DiscountProductsApi.md#getDiscountProduct) | **GET** /discounts/discount-products/{id} | Получение объекта типа DiscountProduct
[**searchDiscountProducts**](DiscountProductsApi.md#searchDiscountProducts) | **POST** /discounts/discount-products:search | Поиск объектов типа DiscountProduct



## getDiscountProduct

> \Ensi\MarketingClient\Dto\DiscountProductResponse getDiscountProduct($id)

Получение объекта типа DiscountProduct

Получение объекта типа DiscountProduct

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getDiscountProduct($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountProductsApi->getDiscountProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountProductResponse**](../Model/DiscountProductResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDiscountProducts

> \Ensi\MarketingClient\Dto\SearchDiscountProductsResponse searchDiscountProducts($search_discount_products_request)

Поиск объектов типа DiscountProduct

Поиск объектов типа DiscountProduct

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discount_products_request = new \Ensi\MarketingClient\Dto\SearchDiscountProductsRequest(); // \Ensi\MarketingClient\Dto\SearchDiscountProductsRequest | 

try {
    $result = $apiInstance->searchDiscountProducts($search_discount_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountProductsApi->searchDiscountProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discount_products_request** | [**\Ensi\MarketingClient\Dto\SearchDiscountProductsRequest**](../Model/SearchDiscountProductsRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchDiscountProductsResponse**](../Model/SearchDiscountProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


# Ensi\MarketingClient\DiscountRelationsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDiscountProducts**](DiscountRelationsApi.md#deleteDiscountProducts) | **DELETE** /discounts/discounts/{id}/products | Удаление заданных продуктов в скидке
[**patchDiscountProducts**](DiscountRelationsApi.md#patchDiscountProducts) | **POST** /discounts/discounts/{id}/products | Обновление только заданных продуктов в скидке



## deleteDiscountProducts

> \Ensi\MarketingClient\Dto\EmptyDataResponse deleteDiscountProducts($id, $delete_discount_products_request)

Удаление заданных продуктов в скидке

Удаление заданных продуктов в скидке

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountRelationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$delete_discount_products_request = new \Ensi\MarketingClient\Dto\DeleteDiscountProductsRequest(); // \Ensi\MarketingClient\Dto\DeleteDiscountProductsRequest | 

try {
    $result = $apiInstance->deleteDiscountProducts($id, $delete_discount_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountRelationsApi->deleteDiscountProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **delete_discount_products_request** | [**\Ensi\MarketingClient\Dto\DeleteDiscountProductsRequest**](../Model/DeleteDiscountProductsRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDiscountProducts

> \Ensi\MarketingClient\Dto\EmptyDataResponse patchDiscountProducts($id, $patch_discount_products_request)

Обновление только заданных продуктов в скидке

Обновление только заданных продуктов в скидке

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountRelationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_discount_products_request = new \Ensi\MarketingClient\Dto\PatchDiscountProductsRequest(); // \Ensi\MarketingClient\Dto\PatchDiscountProductsRequest | 

try {
    $result = $apiInstance->patchDiscountProducts($id, $patch_discount_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountRelationsApi->patchDiscountProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_discount_products_request** | [**\Ensi\MarketingClient\Dto\PatchDiscountProductsRequest**](../Model/PatchDiscountProductsRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


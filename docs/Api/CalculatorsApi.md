# Ensi\MarketingClient\CalculatorsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**calculateCatalog**](CalculatorsApi.md#calculateCatalog) | **POST** /calculators/catalog | Рассчитать скидки для каталога
[**calculateCheckout**](CalculatorsApi.md#calculateCheckout) | **POST** /calculators/checkout | Рассчитать скидки для чекаута



## calculateCatalog

> \Ensi\MarketingClient\Dto\CalculateCatalogResponse calculateCatalog($calculate_catalog_request)

Рассчитать скидки для каталога

Рассчитать скидки для каталога

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\CalculatorsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$calculate_catalog_request = new \Ensi\MarketingClient\Dto\CalculateCatalogRequest(); // \Ensi\MarketingClient\Dto\CalculateCatalogRequest | 

try {
    $result = $apiInstance->calculateCatalog($calculate_catalog_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CalculatorsApi->calculateCatalog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculate_catalog_request** | [**\Ensi\MarketingClient\Dto\CalculateCatalogRequest**](../Model/CalculateCatalogRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\CalculateCatalogResponse**](../Model/CalculateCatalogResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## calculateCheckout

> \Ensi\MarketingClient\Dto\CalculateCheckoutResponse calculateCheckout($calculate_checkout_request)

Рассчитать скидки для чекаута

Рассчитать скидки для чекаута

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\CalculatorsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$calculate_checkout_request = new \Ensi\MarketingClient\Dto\CalculateCheckoutRequest(); // \Ensi\MarketingClient\Dto\CalculateCheckoutRequest | 

try {
    $result = $apiInstance->calculateCheckout($calculate_checkout_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CalculatorsApi->calculateCheckout: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculate_checkout_request** | [**\Ensi\MarketingClient\Dto\CalculateCheckoutRequest**](../Model/CalculateCheckoutRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\CalculateCheckoutResponse**](../Model/CalculateCheckoutResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


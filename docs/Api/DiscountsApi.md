# Ensi\MarketingClient\DiscountsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDiscount**](DiscountsApi.md#createDiscount) | **POST** /discounts/discounts | Создание объекта типа Discount
[**deleteDiscount**](DiscountsApi.md#deleteDiscount) | **DELETE** /discounts/discounts/{id} | Удаление объекта типа Discount
[**getDiscount**](DiscountsApi.md#getDiscount) | **GET** /discounts/discounts/{id} | Получение объекта типа Discount
[**massDiscountsStatusUpdate**](DiscountsApi.md#massDiscountsStatusUpdate) | **POST** /discounts/discounts:mass-status-update | Массовое обновление статусов для объектов типа Discount
[**patchDiscount**](DiscountsApi.md#patchDiscount) | **PATCH** /discounts/discounts/{id} | Обновления части полей объекта типа Discount
[**searchDiscounts**](DiscountsApi.md#searchDiscounts) | **POST** /discounts/discounts:search | Поиск объектов типа Discount



## createDiscount

> \Ensi\MarketingClient\Dto\DiscountResponse createDiscount($create_discount_request)

Создание объекта типа Discount

Создание объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_discount_request = new \Ensi\MarketingClient\Dto\CreateDiscountRequest(); // \Ensi\MarketingClient\Dto\CreateDiscountRequest | 

try {
    $result = $apiInstance->createDiscount($create_discount_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->createDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_discount_request** | [**\Ensi\MarketingClient\Dto\CreateDiscountRequest**](../Model/CreateDiscountRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountResponse**](../Model/DiscountResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDiscount

> \Ensi\MarketingClient\Dto\EmptyDataResponse deleteDiscount($id)

Удаление объекта типа Discount

Удаление объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDiscount($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->deleteDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscount

> \Ensi\MarketingClient\Dto\DiscountResponse getDiscount($id, $include)

Получение объекта типа Discount

Получение объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getDiscount($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->getDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\MarketingClient\Dto\DiscountResponse**](../Model/DiscountResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massDiscountsStatusUpdate

> \Ensi\MarketingClient\Dto\EmptyDataResponse massDiscountsStatusUpdate($mass_discounts_status_update_request)

Массовое обновление статусов для объектов типа Discount

Массовое обновление статусов для объектов типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mass_discounts_status_update_request = new \Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest(); // \Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest | 

try {
    $result = $apiInstance->massDiscountsStatusUpdate($mass_discounts_status_update_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->massDiscountsStatusUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mass_discounts_status_update_request** | [**\Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest**](../Model/MassDiscountsStatusUpdateRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDiscount

> \Ensi\MarketingClient\Dto\DiscountResponse patchDiscount($id, $patch_discount_request)

Обновления части полей объекта типа Discount

Обновления части полей объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_discount_request = new \Ensi\MarketingClient\Dto\PatchDiscountRequest(); // \Ensi\MarketingClient\Dto\PatchDiscountRequest | 

try {
    $result = $apiInstance->patchDiscount($id, $patch_discount_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->patchDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_discount_request** | [**\Ensi\MarketingClient\Dto\PatchDiscountRequest**](../Model/PatchDiscountRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountResponse**](../Model/DiscountResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDiscounts

> \Ensi\MarketingClient\Dto\SearchDiscountsResponse searchDiscounts($search_discounts_request)

Поиск объектов типа Discount

Поиск объектов типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discounts_request = new \Ensi\MarketingClient\Dto\SearchDiscountsRequest(); // \Ensi\MarketingClient\Dto\SearchDiscountsRequest | 

try {
    $result = $apiInstance->searchDiscounts($search_discounts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->searchDiscounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discounts_request** | [**\Ensi\MarketingClient\Dto\SearchDiscountsRequest**](../Model/SearchDiscountsRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchDiscountsResponse**](../Model/SearchDiscountsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


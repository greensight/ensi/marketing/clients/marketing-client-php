# Ensi\MarketingClient\PromoCodesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkPromoCode**](PromoCodesApi.md#checkPromoCode) | **POST** /promo-codes/promo-codes:check | Проверка активности промокода
[**createPromoCode**](PromoCodesApi.md#createPromoCode) | **POST** /promo-codes/promo-codes | Создание объекта типа PromoCode
[**deletePromoCode**](PromoCodesApi.md#deletePromoCode) | **DELETE** /promo-codes/promo-codes/{id} | Удаление объекта типа PromoCode
[**getPromoCode**](PromoCodesApi.md#getPromoCode) | **GET** /promo-codes/promo-codes/{id} | Получение объекта типа PromoCode
[**patchPromoCode**](PromoCodesApi.md#patchPromoCode) | **PATCH** /promo-codes/promo-codes/{id} | Обновления части полей объекта типа PromoCode
[**searchPromoCodes**](PromoCodesApi.md#searchPromoCodes) | **POST** /promo-codes/promo-codes:search | Поиск объектов типа PromoCode



## checkPromoCode

> \Ensi\MarketingClient\Dto\CheckPromoCodeResponse checkPromoCode($check_promo_code_request)

Проверка активности промокода

Проверка активности промокода

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\PromoCodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$check_promo_code_request = new \Ensi\MarketingClient\Dto\CheckPromoCodeRequest(); // \Ensi\MarketingClient\Dto\CheckPromoCodeRequest | 

try {
    $result = $apiInstance->checkPromoCode($check_promo_code_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromoCodesApi->checkPromoCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **check_promo_code_request** | [**\Ensi\MarketingClient\Dto\CheckPromoCodeRequest**](../Model/CheckPromoCodeRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\CheckPromoCodeResponse**](../Model/CheckPromoCodeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createPromoCode

> \Ensi\MarketingClient\Dto\PromoCodeResponse createPromoCode($create_promo_code_request)

Создание объекта типа PromoCode

Создание объекта типа PromoCode

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\PromoCodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_promo_code_request = new \Ensi\MarketingClient\Dto\CreatePromoCodeRequest(); // \Ensi\MarketingClient\Dto\CreatePromoCodeRequest | 

try {
    $result = $apiInstance->createPromoCode($create_promo_code_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromoCodesApi->createPromoCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_promo_code_request** | [**\Ensi\MarketingClient\Dto\CreatePromoCodeRequest**](../Model/CreatePromoCodeRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\PromoCodeResponse**](../Model/PromoCodeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePromoCode

> \Ensi\MarketingClient\Dto\EmptyDataResponse deletePromoCode($id)

Удаление объекта типа PromoCode

Удаление объекта типа PromoCode

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\PromoCodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePromoCode($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromoCodesApi->deletePromoCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPromoCode

> \Ensi\MarketingClient\Dto\PromoCodeResponse getPromoCode($id)

Получение объекта типа PromoCode

Получение объекта типа PromoCode

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\PromoCodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getPromoCode($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromoCodesApi->getPromoCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\PromoCodeResponse**](../Model/PromoCodeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPromoCode

> \Ensi\MarketingClient\Dto\PromoCodeResponse patchPromoCode($id, $patch_promo_code_request)

Обновления части полей объекта типа PromoCode

Обновления части полей объекта типа PromoCode

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\PromoCodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_promo_code_request = new \Ensi\MarketingClient\Dto\PatchPromoCodeRequest(); // \Ensi\MarketingClient\Dto\PatchPromoCodeRequest | 

try {
    $result = $apiInstance->patchPromoCode($id, $patch_promo_code_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromoCodesApi->patchPromoCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_promo_code_request** | [**\Ensi\MarketingClient\Dto\PatchPromoCodeRequest**](../Model/PatchPromoCodeRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\PromoCodeResponse**](../Model/PromoCodeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPromoCodes

> \Ensi\MarketingClient\Dto\SearchPromoCodesResponse searchPromoCodes($search_promo_codes_request)

Поиск объектов типа PromoCode

Поиск объектов типа PromoCode

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\PromoCodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_promo_codes_request = new \Ensi\MarketingClient\Dto\SearchPromoCodesRequest(); // \Ensi\MarketingClient\Dto\SearchPromoCodesRequest | 

try {
    $result = $apiInstance->searchPromoCodes($search_promo_codes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromoCodesApi->searchPromoCodes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_promo_codes_request** | [**\Ensi\MarketingClient\Dto\SearchPromoCodesRequest**](../Model/SearchPromoCodesRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchPromoCodesResponse**](../Model/SearchPromoCodesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

